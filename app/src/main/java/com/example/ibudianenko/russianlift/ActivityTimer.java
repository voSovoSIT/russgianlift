package com.example.ibudianenko.russianlift;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.view.View;
import android.widget.Chronometer;
import android.app.Activity;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by i.budianenko on 26.04.2018.
 */

public class ActivityTimer extends Activity {
    private Chronometer mChronometer;
    private boolean status_start;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        status_start=false;
        mChronometer =(Chronometer) findViewById(R.id.textViewCronometr);
        mChronometer.setText("00");
        mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long elapsedMillis = SystemClock.elapsedRealtime() - mChronometer.getBase();
                Date date= new Date(elapsedMillis);
                DateFormat formatter=new SimpleDateFormat("ss");
                String dateformated=formatter.format(date);
                mChronometer.setText(dateformated);

                /*if (elapsedMillis ==22500) {
                    String strElapsedMillis = "Прошло больше 5 секунд";
                    Toast.makeText(getApplicationContext(),
                            strElapsedMillis, Toast.LENGTH_SHORT)
                            .show();
                }*/
                if (elapsedMillis>45000){
                    mChronometer.stop();
                    //long[] pattern = { 500, 300, 400, 200 };
                    Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    //vibrator.vibrate(pattern, 1);
                    vibrator.vibrate(500);
                }
            }
        });

        mChronometer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invertor();
                if(status_start){
                    onStartClick();
                }
                else {
                    onStopClick();
                }
            }
        });
    }

    public void onStartClick() {
        mChronometer.setBase(SystemClock.elapsedRealtime());
        mChronometer.start();
    }

    public void onStopClick() {
        mChronometer.stop();
    }

    public void onResetClick(View view) {
        mChronometer.setBase(SystemClock.elapsedRealtime());
    }
    public void invertor(){
        if (status_start){status_start=false;}else {status_start=true;};
    }

}
