package com.example.ibudianenko.russianlift;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TableLayout;
import android.view.ViewGroup.LayoutParams;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.Math.*;
import android.view.inputmethod.InputMethodManager;

public class MainActivity extends AppCompatActivity {
    public static double maxSquat=1;
    public static double maxBeanch=1;
    public static double maxDeadLift=1;
    public static double wheight=1;

    EditText editMaxSQ;
    EditText editMaxBP;
    EditText editMaxDL;
    Button buttonCalc;
    EditText editWheightNow;
    TextView textTotalNow;
    TextView textWilksNow;
    TextView textNormativ;
    TableLayout tableProg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonCalc=(Button)findViewById(R.id.buttonCalc);
        editMaxSQ=(EditText)findViewById(R.id.EditSq);
        editMaxBP=(EditText)findViewById(R.id.EditBp);
        editMaxDL=(EditText)findViewById(R.id.EditDL);
        editWheightNow=(EditText)findViewById(R.id.EditWheight);
        textTotalNow=(TextView)findViewById(R.id.textTotalNow);
        textWilksNow=(TextView)findViewById(R.id.textWilksNow);
        textNormativ=(TextView)findViewById(R.id.textNormativ);
        tableProg=(TableLayout)findViewById(R.id.mainTable);
        tableProg.setStretchAllColumns(true);
        checkUserInfo();
        createList(maxSquat,maxBeanch,maxDeadLift);
        refreshInfo();
        getWindow().setSoftInputMode(1);

        
    }

    public void OnClickTableProcent(View v){
        Intent intent = new Intent(MainActivity.this, ProcentActivity.class);
        startActivity(intent);
    }

    public void OnClickPulseTapTap(View v){
        Intent intent = new Intent(MainActivity.this, ActivityPulse.class);
        startActivity(intent);
    }
    public void OnClickTimer(View v){
        Intent intent = new Intent(MainActivity.this, ActivityTimer.class);
        startActivity(intent);
    }


    public void OnClickCalculate(View v){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        //Получаем макс присед
        try {
            String maxSquatS=editMaxSQ.getText().toString();
            maxSquat=Double.parseDouble(maxSquatS);
        }catch (Exception e){}
        //Получаем макс жим
        try {
            String maxBpS = editMaxBP.getText().toString();
            maxBeanch = Double.parseDouble(maxBpS);
        }catch (Exception e){}
        //Получаем макс тягу
        try {
            String maxDlS = editMaxDL.getText().toString();
            maxDeadLift = Double.parseDouble(maxDlS);
        }catch (Exception e){}
        //Получаем вес
        try {
            String wheightStr = editWheightNow.getText().toString();
            wheight = Double.parseDouble(wheightStr);
        }catch (Exception e){}
        try {
            writeFile(maxSquat, maxBeanch, maxDeadLift, wheight);
        }catch (Exception e){}
        refreshInfo();
        createList(maxSquat,maxBeanch,maxDeadLift);
    }



    public void refreshInfo(){
        double total=maxSquat+maxBeanch+maxDeadLift;
        textTotalNow.setText("Сумма "+Double.toString(total)+" кг");
        String wilks=Double.toString(coefWilks(wheight));
        textWilksNow.setText("Вилкс "+wilks);
        String normIPFeq=AddFunction.resultIPF(Normativ.normativIPF,Normativ.manIPF,Normativ.womIPF,1,wheight,total,"IPFэк");
        String normIPFcl=AddFunction.resultIPF(Normativ.normativIPF,Normativ.manIPFclassic,Normativ.womIPF,1,wheight,total,"IPF");
        String normIPFbp=AddFunction.resultIPF(Normativ.normativIPF,Normativ.manIPFbench,Normativ.womIPF,1,wheight,maxBeanch,"IPFжим");

        textNormativ.setText(normIPFeq+"\n"+normIPFcl+"\n"+normIPFbp);
    }

    public void createList(double sQ, double bP, double dL){
        tableProg.removeAllViews();
        //шапка 1 строка
        TableRow tableRow1 = new TableRow(this);
        TextView textRow1=new TextView(this);
        textRow1.setTextSize(20);
        textRow1.setText("");
        tableRow1.addView(textRow1,0);
        TextView textRow12=new TextView(this);
        textRow12.setText("День 1");
        tableRow1.addView(textRow12,1);
        TextView textRow13=new TextView(this);
        textRow13.setText("День 2");
        tableRow1.addView(textRow13,2);
        TextView textRow14=new TextView(this);
        textRow14.setText("День 3");
        tableRow1.addView(textRow14,3);

        View dividerView = new View(this);
        TableLayout.LayoutParams lp = new TableLayout.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT, 2);
        dividerView.setBackgroundColor(getResources().getColor(R.color.colorBlack));
        dividerView.setLayoutParams(lp);



        String [][] matrixProgram=AddFunction.schemaRushianCircle(sQ,bP,dL);

        tableProg.addView(tableRow1);
        tableProg.addView(dividerView);
        //2 строка
        int j=1;
        for(int i=0;i<9;i++) {
            TableRow tableRow2 = new TableRow(this);
            TextView textRow21 = new TextView(this);
            textRow21.setText("Неделя "+j);
            tableRow2.addView(textRow21, 0);

            TextView textRow22 = new TextView(this);
            textRow22.setText("Присед "+matrixProgram[i][0]);
            tableRow2.addView(textRow22, 1);

            TextView textRow23 = new TextView(this);
            textRow23.setText("Становая ");
            tableRow2.addView(textRow23, 2);

            TextView textRow24 = new TextView(this);
            textRow24.setText("Присед "+matrixProgram[i][3]);
            if (i==8){
                textRow24.setBackgroundColor(getResources().getColor(R.color.colorGreyRed));
            }
            tableRow2.addView(textRow24, 3);

            tableProg.addView(tableRow2);
            TableRow tableRow3 = new TableRow(this);
            TextView textRow31 = new TextView(this);
            textRow31.setText("");
            tableRow3.addView(textRow31, 0);

            TextView textRow32 = new TextView(this);
            textRow32.setText("Жим "+matrixProgram[i][1]);
            if (i==8){
                textRow32.setBackgroundColor(getResources().getColor(R.color.colorGreyRed));
            }
            tableRow3.addView(textRow32, 1);

            TextView textRow33 = new TextView(this);
            textRow33.setText(matrixProgram[i][2]);
            if (i==8){
                textRow33.setBackgroundColor(getResources().getColor(R.color.colorGreyRed));
            }
            tableRow3.addView(textRow33, 2);

            TextView textRow34 = new TextView(this);
            textRow34.setText("Жим "+matrixProgram[i][4]);
            tableRow3.addView(textRow34, 3);

            tableProg.addView(tableRow3);

            View dividerView2 = new View(this);
            TableLayout.LayoutParams lp2 = new TableLayout.LayoutParams(
                    ViewGroup.LayoutParams.FILL_PARENT, 2);
            dividerView2.setBackgroundColor(getResources().getColor(R.color.colorBlack));
            dividerView2.setLayoutParams(lp2);
            tableProg.addView(dividerView2);

            j=j+1;
        }


    }

    //Расчет вилкса
    public double coefWilks(double x){
        double total=maxSquat+maxBeanch+maxDeadLift;
        //Коэффициенты мужчин
        double a=-216.0475144;
        double b=16.2606339;
        double c=-0.002388645;
        double d=-0.00113732;
        double e=7.01863E-06;
        double f=-1.291E-08;

        //Женщины
        //double a=594.31747775582;
        //double b=-27.23842536447;
        //double c=0.82112226871;
        //double d=-0.00930733913;
        //double e=0.00004731582;
        //double f=-0.00000009054;


        double coeff=(500/(((((a+(b*x))+(c*Math.pow(x,2)))+(d*Math.pow(x,3)))+(e*Math.pow(x,4)))+(f*Math.pow(x,5))))*total;
        coeff=Math.rint(coeff*100)/100;//Округляем

        return coeff;

    }

    public void checkUserInfo(){
        if (maxSquat==1){
            try {
                FileInputStream fis;
                fis = openFileInput("UserData");
                byte[] bytes = new byte[fis.available()];
                fis.read(bytes);
                String result = new String(bytes);
                String [] resultArr=result.split(",");
                maxSquat=Double.parseDouble(resultArr[0]);
                maxBeanch=Double.parseDouble(resultArr[1]);
                maxDeadLift=Double.parseDouble(resultArr[2]);
                wheight=Double.parseDouble(resultArr[3]);

                editMaxSQ.setText(resultArr[0]);
                editMaxBP.setText(resultArr[1]);
                editMaxDL.setText(resultArr[2]);
                editWheightNow.setText(resultArr[3]);

            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public void writeFile(double sq,double bp,double dl,double wheight) throws Exception{
        String filename="UserData";
        String result=Double.toString(sq)+","+Double.toString(bp)+","+Double.toString(dl)+","+Double.toString(wheight);
        FileOutputStream fos =openFileOutput(filename, Context.MODE_PRIVATE);
        fos.write(result.getBytes());
        fos.close();
    }

}
