package com.example.ibudianenko.russianlift;

/**
 * Created by i.budianenko on 21.09.2017.
 */

public class AddFunction {

    public static String [][] schemaRushianCircle(Double sq, Double bp, Double dl){

        Double matrix[][]={ {roundDb(0.8*sq),roundDb(0.8*bp),roundDb(0.8*dl),roundDb(0.8*sq),roundDb(0.8*bp)},
                {roundDb(0.8*sq),roundDb(0.8*bp),roundDb(0.8*dl),roundDb(0.8*sq),roundDb(0.8*bp)},
                {roundDb(0.8*sq),roundDb(0.8*bp),roundDb(0.8*dl),roundDb(0.8*sq),roundDb(0.8*bp)},
                {roundDb(0.8*sq),roundDb(0.8*bp),roundDb(0.8*dl),roundDb(0.8*sq),roundDb(0.8*bp)},
                {roundDb(0.8*sq),roundDb(0.85*bp),roundDb(0.85*dl),roundDb(0.85*sq),roundDb(0.8*bp)},
                {roundDb(0.8*sq),roundDb(0.9*bp),roundDb(0.9*dl),roundDb(0.9*sq),roundDb(0.8*bp)},
                {roundDb(0.8*sq),roundDb(0.95*bp),roundDb(0.95*dl),roundDb(0.95*sq),roundDb(0.8*bp)},
                {roundDb(0.8*sq),roundDb(bp),roundDb(dl),roundDb(sq),roundDb(0.8*bp)},
                {roundDb(0.8*sq),roundDb(1.05*bp),roundDb(1.05*dl),roundDb(1.05*sq),roundDb(0.8*bp)}
        };

        String matrixStr [][]={{"6x2","6x3","6x2","6x3","6x2"},
                {"6x2","6x4","6x4","6x4","6x2"},
                {"6x2","6x5","6x5","6x5","6x2"},
                {"6x2","6x5","6x6","6x6","6x2"},
                {"6x2","5x5","5x5","5x5","6x2"},
                {"6x2","4x4","4x4","4x4","6x2"},
                {"6x2","3x3","3x3","3x3","6x2"},
                {"6x2","2x2","2x2","2x2","6x2"},
                {"6x2","1x1","1x1","1x1","6x2"}
        };


        String matrixNew[][]=new String [9][5];
        for (int i=0;i<9;i++){
            for(int j=0;j<5;j++){
                matrixNew[i][j]=matrix[i][j].toString().replace(".0","")+"x"+matrixStr[i][j];
                System.out.print(matrixNew[i][j]+" ");
            }
            System.out.println("\n");
        }

        return matrixNew;
    }

    public static Double roundDb(Double number){

        number = Math.floor(number);
        if((number%10==2)|(number%10==7))
        {number=number+0.5;}
        else if(number%10==1)
        {number=number-1;}
        else if(number%10==3)
        {number=number-0.5;}
        else if(number%10==4)
        {number=number+1;}
        else if(number%10==6)
        {number=number-1;}
        else if(number%10==8)
        {number=number-0.5;}
        else if(number%10==9)
        {number=number+1;}
        return number;
    }




    public static String resultIPF(String [] normativ,Double[][]man,Double[][]wom, int gender, Double wheight, Double total, String nameFed)
    {
        Double matrix[][];
        //Опеделяем пол для таблицы
        if (gender==1){
            matrix=man;
        }else {matrix=wom;}
        int size1= matrix.length;
        int choose=0;
        int chooseRang=0;
        String rang="";
        String nRang="";
        Double difTotal=0.0;
        //Определяем весовую категорию
        for (int i=0;matrix.length>i;i++){
            if (wheight<=Normativ.manIPF[i][0]){
                System.out.println(i);
                choose=i;
                break;
            }
            else {
                choose=size1-1;
            }
        }
        for (int i=1;matrix[1].length>i;i++){

            if (total>=matrix[choose][i]){
                System.out.println(matrix[choose][i]);
                chooseRang=i-1;
                rang=normativ[chooseRang];
                if (!rang.equals("МСМК")){
                    nRang=normativ[chooseRang-1];
                    difTotal=matrix[choose][i-1]-total;
                }
                return rang+" "+nameFed+" до "+nRang+" "+difTotal+"кг" ;
            }
            else
            {
                rang="";
            }
        }
        return rang;
    }

}
