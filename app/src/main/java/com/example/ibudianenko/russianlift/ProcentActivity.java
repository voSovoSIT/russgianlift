package com.example.ibudianenko.russianlift;

import android.support.v7.app.AppCompatActivity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by i.budianenko on 10.01.2018.
 */

public class ProcentActivity  extends AppCompatActivity {

    TextView textProcentName;
    TableLayout tableProcent;

    Double matrixProc[]={10.0,20.0,30.0,40.0,50.0,60.0,65.0,70.0,75.0,80.0,85.0,95.0,100.0,105.0,110.0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_procent_table);
        textProcentName=(TextView)findViewById(R.id.textProcentName);
        tableProcent=(TableLayout)findViewById(R.id.procentTable);
        tableProcent.setStretchAllColumns(true);

        textProcentName.setText("Приседание "+MainActivity.maxSquat+" кг "+"Жим "+MainActivity.maxBeanch+" кг "+"Тяга "+MainActivity.maxDeadLift+" кг");

        createListProcent(MainActivity.maxSquat,MainActivity.maxBeanch,MainActivity.maxDeadLift);
    }

    public void createListProcent(Double sq,Double bp,Double dl){
        tableProcent.removeAllViews();

        //шапка 1 строка
        TableRow tableRow1 = new TableRow(this);
        TextView textRow1=new TextView(this);
        textRow1.setTextSize(20);
        textRow1.setText("    %");
        tableRow1.addView(textRow1,0);
        TextView textRow12=new TextView(this);
        textRow12.setText("Приседание");
        tableRow1.addView(textRow12,1);
        TextView textRow13=new TextView(this);
        textRow13.setText("Жим");
        tableRow1.addView(textRow13,2);
        TextView textRow14=new TextView(this);
        textRow14.setText("Тяга");
        tableRow1.addView(textRow14,3);

        //Разделитель
        View dividerView = new View(this);
        TableLayout.LayoutParams lp = new TableLayout.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT, 2);
        dividerView.setBackgroundColor(getResources().getColor(R.color.colorBlack));
        dividerView.setLayoutParams(lp);

        tableProcent.addView(tableRow1);
        tableProcent.addView(dividerView);

        int sizeMatrix=matrixProc.length;
        for (int i=0;i<sizeMatrix;i++){

            TableRow tableRow2 = new TableRow(this);
            if (i<6){
                tableRow2.setBackgroundColor(getResources().getColor(R.color.colorYelow));
            }
            else if ((i<10)&(i>5)){
                tableRow2.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
            else if ((i<13)&(i>9)){
                tableRow2.setBackgroundColor(getResources().getColor(R.color.colorOrange));
            }
            else if (i>11){
                tableRow2.setBackgroundColor(getResources().getColor(R.color.colorRed));
            }
            TextView textRow21 = new TextView(this);
            textRow21.setText("     "+AddFunction.roundDb(matrixProc[i]).toString());
            tableRow2.addView(textRow21, 0);


            TextView textRow22 = new TextView(this);
            Double res1=AddFunction.roundDb((matrixProc[i]/100.0)*sq);
            textRow22.setText(res1.toString());
            tableRow2.addView(textRow22, 1);

            TextView textRow23 = new TextView(this);
            Double res2=AddFunction.roundDb((matrixProc[i]/100.0)*bp);
            textRow23.setText(res2.toString());
            tableRow2.addView(textRow23, 2);

            TextView textRow24 = new TextView(this);
            Double res3=AddFunction.roundDb((matrixProc[i]/100.0)*dl);
            textRow24.setText(res3.toString());
            tableRow2.addView(textRow24, 3);

            tableProcent.addView(tableRow2);
        }



    }
}
