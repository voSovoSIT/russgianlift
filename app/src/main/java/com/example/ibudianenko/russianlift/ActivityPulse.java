package com.example.ibudianenko.russianlift;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


/**
 * Created by i.budianenko on 24.04.2018.
 */

public class ActivityPulse extends AppCompatActivity {
    int status;
    //Status
    final int STATE_ONE=1;
    final int STATE_TWO=2;
    final int STATE_RESULT_ONE=9;
    final int STATE_RESULT_TWO=8;
    //
    long start;
    long end;

    TextView textMeasure;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulse);
        status=STATE_ONE;
        textMeasure=(TextView)findViewById(R.id.textViewMeasur);
        textMeasure.setText("1");
        textMeasure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status==STATE_ONE){
                    status=STATE_TWO;
                    textMeasure.setText("2");
                    checkLight();
                    start=System.nanoTime();
                    end=0;
                }
                else if (status==STATE_TWO){
                    status=STATE_RESULT_ONE;
                    end=System.nanoTime();
                    textMeasure.setText(resultPuls(start,end));
                    checkLight();
                }
                else {
                    status=STATE_TWO;
                    textMeasure.setText("tap");
                    checkLight();
                    start=System.nanoTime();
                    end=0;
                }
            }
        });
    }

    public String resultPuls(long start,long end){
        long traceTime=end-start;
        Double traceTimeSec=(double)traceTime/1000000000.0;
        Double resultSec=60/traceTimeSec;
        int result=resultSec.intValue();
        String res=String.valueOf(result);
        return res;
    }

    public void checkLight(){
        if (textMeasure.getText().length()==3){
            textMeasure.setTextSize(1,180);
        }
        else textMeasure.setTextSize(1,320);
    }

}

